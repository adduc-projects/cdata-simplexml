CData for SimpleXMLElement
===

This library adds a wrapper around the SimpleXMLElement class to allow creating
CDATA elements.

Usage
---

```php
<?php

use Adduc\SimpleXMLElement;

$data = '<test/>';
$xml = new SimpleXMLElement($data);
$xml->addCData('element', 'Test Data');
echo $xml->asXml();
// <test><element><![CDATA[Test Data]]></element></test>
```
