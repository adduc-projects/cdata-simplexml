<?php declare(strict_types=1);

namespace Adduc;

class SimpleXMLElement extends \SimpleXMLElement
{
    public function addCData(string $element, string $cdata_text): self
    {
        $element = $this->addChild($element);
        $node = dom_import_simplexml($element);
        $no = $node->ownerDocument;
        $node->appendChild($no->createCDATASection($cdata_text));
        return $element;
    }
}
