<?php declare(strict_types=1);

namespace Adduc;

use PHPUnit\Framework\TestCase;

class SimpleXMLElementTest extends TestCase
{
    /**
     * @dataProvider provideAddCData
     */
    public function testAddCData(string $element, string $data, string $expected): void
    {
        $xml = new SimpleXMLElement('<test/>');
        $xml->addCData($element, $data);

        $this->assertEquals($expected, $xml->element->asXml());
    }

    public function provideAddCData(): array
    {
        $tests = [
            ['element', 'Test Data', '<element><![CDATA[Test Data]]></element>'],
        ];

        return $tests;
    }
}
